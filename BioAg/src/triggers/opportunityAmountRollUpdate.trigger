trigger opportunityAmountRollUpdate on OpportunityLineItem (after insert, after update, before delete) {


    /* variable Declaretion */
    
    set<Id> opportunityId= new set<Id>();
    set<Id> accountId= new set<Id>();
    set<Id> accountDelId= new set<Id>();
    Map<Id,Account> accountOfMap=new Map<Id,Account>();
    Map<Id,Account> accountDelOfMap=new Map<Id,Account>();
    Map<Id,Opportunity> opportunityOfMap=new Map<Id,Opportunity>();
    Map<Id,OpportunityLineItem> opportunityLineOldList=new Map<Id,OpportunityLineItem>();
    Map<Id,Opportunity> opportunityDelOfMap=new Map<Id,Opportunity>();
    Map<Id, Account> newAccountRollAmount=new Map<Id, Account>();
    List<Task> newAccountTask=new List<Task>();
    
    
    
    try{
        if(!trigger.isDelete){     
        /* Set of Opportunity Id */
        
        for(OpportunityLineItem opportunityLineList:Trigger.new){
            opportunityId.add(opportunityLineList.OpportunityId);
        }
        
        
        /* Set of Account Id Map of Opportunity Record */
        
        for(Opportunity opportunityList: [select id, StageName, AccountId, Amount from Opportunity where id IN :opportunityId]){
            if(opportunityList.StageName!='Accepted' || opportunityList.StageName!='Rejected'){
                accountId.add(opportunityList.AccountId);
                opportunityOfMap.put(opportunityList.id,opportunityList);
            }
        }
        
        
        /* Map of Account Record */
        
        for(Account accountlist : [select id,Name,Credit_limit_approved_Amount__c,Open_Opportunity_Amount__c,Open_Opportunity_Total_Amount__c,OwnerId from Account where id IN :accountId])
        {
            accountOfMap.put(accountlist.id,accountlist);
        }
        
       
           
        /* Trigger for Update */
        
        if(Trigger.Isupdate){
            
            
            for(OpportunityLineItem opportunityLineList:Trigger.new){
            
                Opportunity selectedOpportunity = opportunityOfMap.get(opportunityLineList.Opportunityid);
                
                Account selectedAccount =  accountOfMap.get(selectedOpportunity.Accountid);
                
                if(newAccountRollAmount.get(selectedAccount.id) != NULL){
                    newAccountRollAmount.get(selectedAccount.id).Open_Opportunity_Total_Amount__c +=(opportunityLineList.TotalPrice-Trigger.oldmap.get(opportunityLineList.id).TotalPrice) ;
                }else{
                    accountOfMap.get(selectedOpportunity.Accountid).Open_Opportunity_Total_Amount__c +=(opportunityLineList.TotalPrice-Trigger.oldmap.get(opportunityLineList.id).TotalPrice);
                    newAccountRollAmount.put(selectedAccount.id, accountOfMap.get(selectedOpportunity.Accountid));
                }  
                
                if(Trigger.oldmap.get(opportunityLineList.id).TotalPrice!=opportunityLineList.TotalPrice && selectedAccount.Credit_limit_approved_Amount__c < selectedAccount.Open_Opportunity_Total_Amount__c){
                    task createtask = new task(OwnerId=selectedAccount.OwnerId,
                                               Subject='Credit Limit Exceeded',
                                               status = 'Not Started',
                                               ActivityDate=System.Today(),
                                               IsReminderSet = true,
                                               ReminderDateTime = System.now(),
                                               Priority='Normal',
                                               WhatId=selectedAccount.Id,
                                               Description='Credit Limit Exceeded ,either close one of the other opportunities or seek an increase to this clients credit limit ');
                    newAccountTask.add(createtask);
                        
                }     
            }
        }
        
        
        /* Trigger for insert */
        
        if(trigger.isInsert){
           
            for(OpportunityLineItem opportunityLineList:Trigger.new){
                
                Opportunity selectedOpportunity = opportunityOfMap.get(opportunityLineList.Opportunityid);
                
                Account selectedAccount =  accountOfMap.get(selectedOpportunity.Accountid);
                
                
                if(newAccountRollAmount.get(selectedAccount.id) != NULL){
                    newAccountRollAmount.get(selectedAccount.id).Open_Opportunity_Total_Amount__c +=(opportunityLineList.TotalPrice) ;
                }else{
                    accountOfMap.get(selectedOpportunity.Accountid).Open_Opportunity_Total_Amount__c +=(opportunityLineList.TotalPrice);
                    newAccountRollAmount.put(selectedAccount.id, accountOfMap.get(selectedOpportunity.Accountid));
                }
                
                if((selectedAccount.Credit_limit_approved_Amount__c<opportunityLineList.TotalPrice)||(selectedAccount.Credit_limit_approved_Amount__c<selectedAccount.Open_Opportunity_Total_Amount__c)){
                                
                        task createtask = new task(OwnerId=selectedAccount.OwnerId,
                                                       Subject='Credit Limit Exceeded',
                                                       status = 'Not Started',
                                                       ActivityDate=System.Today(),
                                                       Priority='Normal',
                                                       IsReminderSet = true,
                                                       ReminderDateTime = System.now(),
                                                       WhatId=selectedAccount.Id,
                                                       Description='Credit Limit Exceeded ,either close one of the other opportunities or seek an increase to this clients credit limit ');
                        newAccountTask.add(createtask);
                }
            
            }
        }
        
        
        /* Trigger for Delete*/
        
        }else{
            
            for(OpportunityLineItem opportunityLineList:Trigger.old){
            	system.debug('***********************************'+opportunityLineList.id);
                
                opportunityId.add(opportunityLineList.OpportunityId);
                
                
                System.debug('********************************>'+opportunityId.size());
        	}
            
            for(Opportunity opportunityList: [select id, StageName, AccountId, Amount from Opportunity where id IN :opportunityId]){
                if(opportunityList.StageName!='Accepted' || opportunityList.StageName!='Rejected'){
                    accountDelId.add(opportunityList.AccountId);
                    System.debug('********************************>'+accountDelId.size());
                    opportunityDelOfMap.put(opportunityList.id,opportunityList);
                    System.debug('********************************>'+opportunityDelOfMap.size());
                }
            }
            
            for(Account accountlist : [select id,Name,Credit_limit_approved_Amount__c,Open_Opportunity_Amount__c,Open_Opportunity_Total_Amount__c,OwnerId from Account where id IN :accountDelId])
            {
                accountDelOfMap.put(accountlist.id,accountlist);
                System.debug('********************************>'+accountDelOfMap.size());
            }
            
            for(OpportunityLineItem opportunityLineList:Trigger.old){
                
                Opportunity selectedOpportunity = opportunityDelOfMap.get(opportunityLineList.Opportunityid);
                System.debug('********************************>'+selectedOpportunity);
                Account selectedAccount =  accountDelOfMap.get(selectedOpportunity.Accountid);
                System.debug('********************************>'+selectedAccount);
                if(newAccountRollAmount.get(selectedAccount.id) != NULL){
                    newAccountRollAmount.get(selectedAccount.id).Open_Opportunity_Total_Amount__c -=(opportunityLineList.TotalPrice) ;
                }else{
                    accountDelOfMap.get(selectedOpportunity.Accountid).Open_Opportunity_Total_Amount__c -= opportunityLineList.TotalPrice;
                    newAccountRollAmount.put(selectedAccount.id, accountDelOfMap.get(selectedOpportunity.Accountid));
                }
                System.debug('********************************>'+newAccountRollAmount.size());
            }
        }
        
        
        
        /* Update Custom Rollup in Account*/
        
        if(newAccountRollAmount.size()>0){
            
            update newAccountRollAmount.values();
            
        }
        
        
        /* Create Task */
        
        if(newAccountTask.size()>0){
        
            insert newAccountTask;
            
        }
    }catch(Exception de){
        System.debug('Data exception:'+ de.getMessage());
    }
}