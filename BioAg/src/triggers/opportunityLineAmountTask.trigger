trigger opportunityLineAmountTask on OpportunityLineItem (after insert, after update) {


    /* variable Declaretion */
    
    List<Id> opportunityId= new List<Id>();
    List<Id> accountId= new List<Id>();
    Map<Id,Account> accountOfMap=new Map<Id,Account>();
    Map<Id,Opportunity> opportunityOfMap=new Map<Id,Opportunity>();
    List<Task> newAccountTask=new List<Task>();
    
    
   
    
    for(OpportunityLineItem opportunityLineList:Trigger.new){
    
        opportunityId.add(opportunityLineList.OpportunityId);
    }
    
    for(Opportunity opportunityList: [select id, StageName, AccountId from Opportunity where id IN :opportunityId]){
        if(opportunityList.StageName!='Accepted' || opportunityList.StageName!='Rejected'){
            accountId.add(opportunityList.AccountId);
            opportunityOfMap.put(opportunityList.id,opportunityList);
        }
    }
    
    
    for(Account accountlist : [select id,Name,Credit_limit_approved_Amount__c,Open_Opportunity_Amount__c,OwnerId from Account where id IN :accountId])
    {
        accountOfMap.put(accountlist.id,accountlist);
    }
    
    
    /* Trigger for Update */
    
    if(Trigger.Isupdate){
       
        for(OpportunityLineItem opportunityLineList:Trigger.new){
        
            Opportunity selectedOpportunity = opportunityOfMap.get(opportunityLineList.Opportunityid);
            
            Account selectedAccount =  accountOfMap.get(selectedOpportunity.Accountid);
               
            if(Trigger.oldmap.get(opportunityLineList.id).TotalPrice!=opportunityLineList.TotalPrice && selectedAccount.Credit_limit_approved_Amount__c < selectedAccount.Open_Opportunity_Amount__c){
                task createtask = new task(OwnerId=selectedAccount.OwnerId,
                                           Subject='Credit Limit Exceeded For Deal',
                                           status = 'Not Started',
                                           ActivityDate=System.Today(),
                                           IsReminderSet = true,
                                           ReminderDateTime = System.now(),
                                           Priority='Normal',
                                           WhatId=selectedAccount.Id,
                                           Description='Credit Limit Exceeded ,either close one of the other opportunities or seek an increase to this clients credit limit ');
                newAccountTask.add(createtask);
                    
            }
        }
    }
    
    
    /* Trigger for insert */
    
    if(trigger.isInsert){
    
        for(OpportunityLineItem opportunityLineList:Trigger.new){
            
            Opportunity selectedOpportunity = opportunityOfMap.get(opportunityLineList.Opportunityid);
            
            Account selectedAccount =  accountOfMap.get(selectedOpportunity.Accountid);
            
            if((selectedAccount.Credit_limit_approved_Amount__c<opportunityLineList.TotalPrice)||(selectedAccount.Credit_limit_approved_Amount__c<selectedAccount.Open_Opportunity_Amount__c)){
                        
                task createtask = new task(OwnerId=selectedAccount.OwnerId,
                                               Subject='Credit Limit Exceeded For Deal',
                                               status = 'Not Started',
                                               ActivityDate=System.Today(),
                                               Priority='Normal',
                                               IsReminderSet = true,
                                               ReminderDateTime = System.now(),
                                               WhatId=selectedAccount.Id,
                                               Description='Credit Limit Exceeded ,either close one of the other opportunities or seek an increase to this clients credit limit ');
                newAccountTask.add(createtask);
            }
        }
    }
    
    
    /* Create Task */
    
    if(newAccountTask.size()>0){
    
        insert newAccountTask;
        
    }
    
}