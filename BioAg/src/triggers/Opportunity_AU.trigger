trigger Opportunity_AU on Opportunity (after update) 
/*
Trigger to update Account's region when the opportunity owner is changed

Author :Ashwin(Extentor)

Created date : 

Last Modified date :

*/ 
{
Map<Id,String> userRegion = new Map<Id, String>();
Map<Id,Account> listOfaccount = new Map<Id,Account>();

List<Id> accountId = new List<Id> ();
List<Id> ownerId = new List<Id> ();
List<Account> accountsToUpdate = new List<Account> ();
for(Opportunity listOfopportunity : Trigger.new)
        {
                accountid.add(listOfopportunity.AccountId);
                ownerId.add(listOfopportunity.OwnerId);
        }

        for(User listOfuser : [Select id,Region__c from User where Id IN :ownerId])
        {
            userRegion.put(listOfuser.id,listOfuser.Region__c);
          
        }
        
    for(Account acc: [Select id from Account where Id IN :accountId])
    {
        listOfaccount.put(acc.id,acc);
      
    }
 
for(Opportunity opp: Trigger.new)
        
        {
        Opportunity oldopportunity=trigger.oldMap.get(opp.id);
             if(opp.OwnerId !=oldopportunity.OwnerId)
             {
                     Account updateAccounts = listOfaccount.get(opp.AccountId);
                    if(updateAccounts != null)
                    {
                     updateAccounts.Region__c=userRegion.get(opp.OwnerId);
                     accountsToUpdate.add(updateAccounts);
                     }
             
             }
        
        
        }        
                if(accountsToUpdate.size()>0)
        {
         update accountsToUpdate;

         }

}