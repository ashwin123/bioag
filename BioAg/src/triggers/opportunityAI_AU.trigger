trigger opportunityAI_AU  on Opportunity (after insert,after update) 
{
    
    List<Id> Accountid = new List<Id>();
    map<id,Account> mapofAccount = new map<id,Account>();
    List<Task> newtasks = new List<Task>();
    
    for(Opportunity opportunitylist:Trigger.new)
    {
        
        Accountid.add(opportunitylist.Accountid);
        
    }
    
    for(Account accountlist : [select id,Name,Credit_limit_approved_Amount__c,Open_Opportunity_Amount__c,OwnerId from Account where id IN :Accountid])
    {
        mapofAccount.put(accountlist.id,accountlist);
        
    }
    
    if(Trigger.Isupdate)
    {
    
    
        for(Opportunity opportunitylist:Trigger.new)
        {
            
            Account selectedAccount =  mapofAccount.get(opportunitylist.Accountid);
         
            
            if(Trigger.oldmap.get(opportunitylist.id).Amount!=opportunitylist.Amount&&selectedAccount.Credit_limit_approved_Amount__c<selectedAccount.Open_Opportunity_Amount__c)
            {
                task createtask = new task(OwnerId=selectedAccount.OwnerId,
                                           Subject='Credit Limit Exceeded',
                                           status = 'Not Started',
                                           ActivityDate=System.Today(),
                                           IsReminderSet = true,
                                           ReminderDateTime = System.now(),
                                           Priority='Normal',
                                           WhatId=selectedAccount.Id,
                                           Description='Credit Limit Exceeded ,either close one of the other opportunities or seek an increase to this clients credit limit ');
                newtasks.add(createtask);
                
                
                
            }
        }
    }
    if(trigger.isInsert)
    {
    
    System.debug('INSIDE ***************************************************ISINESRT');
        for(Opportunity opportunitylist:Trigger.new)
        {
            
            Account selectedAccount =  mapofAccount.get(opportunitylist.Accountid);
            system.debug('************************************************************OPPAMOUNT'+selectedAccount.Open_Opportunity_Amount__c);
            
            if((selectedAccount.Credit_limit_approved_Amount__c<opportunitylist.Amount)||(selectedAccount.Credit_limit_approved_Amount__c<selectedAccount.Open_Opportunity_Amount__c))
            {
            
             System.debug('INSIDE ***************************************************IFCONDITION');
                task createtask = new task(OwnerId=selectedAccount.OwnerId,
                                           Subject='Credit Limit Exceeded',
                                           status = 'Not Started',
                                           ActivityDate=System.Today(),
                                           Priority='Normal',
                                           IsReminderSet = true,
                                           ReminderDateTime = System.now(),
                                           WhatId=selectedAccount.Id,
                                           Description='Credit Limit Exceeded ,either close one of the other opportunities or seek an increase to this clients credit limit ');
                newtasks.add(createtask);
                
                
                
            }
        }
    }
    if(newtasks.size()>0)
    {
        insert newtasks;
    }
}