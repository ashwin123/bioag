trigger OpportunityRollUpDeduct on Opportunity (after update, before delete) {

    /* variable Declaretion */
    
    Map<Id,Account> accountOfMap=new Map<Id,Account>();
    Map<Id,Account> accountDelOfMap=new Map<Id,Account>();
    Set<Id> accountId=new Set<Id>();
    Set<Id> accountDelId=new Set<Id>();
    List<Account> newAccountUpdate=new List<Account>();
    
    try{
        if(!trigger.isDelete){ 
            /* Set of Account Id */
            
            for(Opportunity opportunityList: Trigger.new){
                accountId.add(opportunityList.AccountId);
            }
           
            
            /* Map of Account Record */
            
            for(Account accountlist : [select id,Name,Credit_limit_approved_Amount__c,Open_Opportunity_Amount__c,Open_Opportunity_Total_Amount__c,OwnerId from Account where id IN :accountId])
            {
                accountOfMap.put(accountlist.id,accountlist);
            }
            
            
            /* Trigger after update */
            
            
            for(Opportunity opportunityList: Trigger.new){
                
                if(opportunityList.StageName=='Accepted' || opportunityList.StageName=='Rejected'){
                    
                    Account selectedAccount = accountOfMap.get(opportunityList.Accountid);
                        
                        
                            selectedAccount.Open_Opportunity_Total_Amount__c -=opportunityList.Amount;
                       
                    
                    newAccountUpdate.add(selectedAccount);
                }
            }
        }else{
            /* Set of Account Id */
            
            for(Opportunity opportunityList: Trigger.old){
                accountDelId.add(opportunityList.AccountId);
            }
           
            
            /* Map of Account Record */
            
            for(Account accountlist : [select id,Name,Credit_limit_approved_Amount__c,Open_Opportunity_Amount__c,Open_Opportunity_Total_Amount__c,OwnerId from Account where id IN :accountDelId])
            {
                accountDelOfMap.put(accountlist.id,accountlist);
            }
            
            for(Opportunity opportunityList: Trigger.old){
                   
                    Account selectedAccount = accountDelOfMap.get(opportunityList.Accountid);
                        
                            selectedAccount.Open_Opportunity_Total_Amount__c -=opportunityList.Amount;
                    
                    newAccountUpdate.add(selectedAccount);
            }
        }
       
        
        /* Update Custom Rollup in Account*/
        
        if(newAccountUpdate.size()>0){
        
            
               update newAccountUpdate;
            
            
        }
    }catch(Exception de){
        System.debug('Data exception:'+ de.getMessage());
    }
}