trigger Quotes_AI_AU on Quote (after insert,after update)
{
    Map<Id,Opportunity> listOpp = new Map<Id,Opportunity>();
    List<Id> listOfOppurtunityId = new List<Id>();
    List<Id> listOfQuotes = new List<Id>();
    Map<Id,String> shipsToContact = new Map<Id,String>();
    Map<Id,String> deliveryAddress = new Map<Id,String>();
    for(Quote q:Trigger.new)
    {
        listOfQuotes.add(q.Id);
        listOfOppurtunityId.add(q.OpportunityId);
    }
    for(Opportunity opp: [Select id from Opportunity where Id IN :listOfOppurtunityId])
    {
        listOpp.put(opp.id,opp);
      
    }
    for(Quote q : [select Id,Name,ShippingName,ShippingStreet, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode from Quote where Id IN:listOfQuotes])
    {
        String delAddress = '';
        if(q.ShippingStreet!=null&&q.ShippingStreet!='')
        {
            delAddress = delAddress + q.ShippingStreet;
        }
        if(q.ShippingCity!=null&&q.ShippingCity!='')
        {
            delAddress = delAddress +'\n'+ q.ShippingCity;
        }
        if(q.ShippingState!=null&&q.ShippingState!='')
        {
            delAddress = delAddress +'\n'+ q.ShippingState;
        }
        if(q.ShippingCountry!=null&&q.ShippingCountry!='')
        {
            delAddress = delAddress +'\n'+ q.ShippingCountry;
        }
        if(q.ShippingPostalCode!=null&&q.ShippingPostalCode!='')
        {
            delAddress = delAddress +'.'+ q.ShippingPostalCode;
        }
        System.debug(q.ShippingName);
        shipsToContact.put(q.Id,q.ShippingName);
        deliveryAddress.put(q.Id,delAddress);
    }
    for(Quote q: Trigger.new)
    {
        Opportunity opp = listOpp.get(q.OpportunityId);
        opp.Ship_to_Name__c = shipsToContact.get(q.Id);
        opp.Delivery_Address__c = deliveryAddress.get(q.Id);
        database.upsert(opp);
    }
}