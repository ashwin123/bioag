<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Notification_to_Sales_Team</fullName>
        <description>Email Notification to Sales Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>sales@bioag.com.au.dev</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Email_Notification_to_Sales</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Deliverable_date</fullName>
        <field>CloseDate</field>
        <formula>TODAY()+30</formula>
        <name>Update Deliverable date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email Notification to Sales Team</fullName>
        <actions>
            <name>Email_Notification_to_Sales_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <description>When opportunity stage is &quot;Accepted&quot; the email notification will send to sales team</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Delivery date</fullName>
        <actions>
            <name>Update_Deliverable_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Delayed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
