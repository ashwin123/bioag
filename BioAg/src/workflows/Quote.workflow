<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Order_Alert</fullName>
        <ccEmails>sales@bioag.com.au</ccEmails>
        <description>Order Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Order_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Default_value_to_GST</fullName>
        <field>Tax</field>
        <formula>0.1</formula>
        <name>Default value to GST</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Order Alert</fullName>
        <actions>
            <name>Order_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <description>Sends an email to sales@bioag.com.au when a Quote has a status of Accepted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
