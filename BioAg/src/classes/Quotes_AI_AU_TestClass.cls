@isTest
public class Quotes_AI_AU_TestClass
{
    static TestMethod void updateOpportunity()
    {
        Account testAccount = new Account();
        testAccount.Name = 'TestAccount';
        database.insert(testAccount);
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'TestOpportunity';
        testOpportunity.Accountid = testAccount.id;
        testOpportunity.CloseDate = date.today();
        testOpportunity.StageName = 'Initial contact';
        database.insert(testOpportunity);
        Quote testQuote = new Quote();
        testQuote.Name = 'TestQuote';
        testQuote.OpportunityId = testOpportunity.id;
        testQuote.Status = 'Draft';
        testQuote.ShippingName = 'TestName';
        testQuote.ShippingStreet = 'TestStreet';
        testQuote.ShippingCity = 'TestCity';
        testQuote.ShippingState = 'TestState';
        testQuote.ShippingCountry = 'TestCountry';
        testQuote.ShippingPostalCode = '8378';
        Test.startTest();
        database.insert(testQuote);
        Test.stopTest();
        Opportunity opp = [select Id,Ship_to_Name__c,Delivery_Address__c from Opportunity where Id=:testOpportunity.id];
        System.assertEquals(opp.Ship_to_Name__c,'TestName');
    }
}