@isTest(seeAllData = false)
Public class updateAccountregion_testclass
{
Static testMethod void updateAccountregion()
{

 
      User oldUser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(),Region__c='South West Victoria', 
      TimeZoneSidKey='America/Los_Angeles', UserName='standardus22er@testorg.com');
      insert oldUser;
      
      User newUser = new User(Alias = 'standt1', Email='standarduser123@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = UserInfo.getProfileId(),Region__c='Sunraysia', 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1234@testorg.com');
      insert  newUser;
      
     Account testAccount=new  Account(name='test',Region__c='South West Victoria');
     insert testAccount;
     
opportunity testOpportunity = new opportunity(name='testopp',  AccountId  = testAccount.Id,StageName='Initial contact',CloseDate=System.Today(),OwnerId=oldUser.Id);
insert testOpportunity;
testOpportunity.Ownerid=newUser.id;
update testOpportunity;
Account updatedAcc = [select Id,Region__c from Account where Id = :testAccount.Id];
system.assertEquals(updatedAcc.Region__c,'Sunraysia'); 

}

}