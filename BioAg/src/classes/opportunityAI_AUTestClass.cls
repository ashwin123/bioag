@isTest
public class opportunityAI_AUTestClass{
    static TestMethod void updateAccount()
    {
    	//Create new Account
        Account testAccount = new Account();
        testAccount.Name = 'TestAccount';
        testAccount.Credit_limit_approved_Amount__c=400;
        database.insert(testAccount);
        
        //Create new Opportunity 
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'TestOpportunity';
        testOpportunity.Accountid = testAccount.id;
        testOpportunity.CloseDate = date.today();
        testOpportunity.StageName = 'Accepted';
        testOpportunity.Amount=5000;
        database.insert(testOpportunity);
               
        //Create new Opportunity 
        Opportunity testOpportunity2 = new Opportunity();
        testOpportunity2.Id = testOpportunity.id;
        testOpportunity2.CloseDate = date.today();
        testOpportunity2.StageName = 'Accepted';
        testOpportunity2.Amount=6000;
        database.Update(testOpportunity2);
        //system.assertEquals(testAccount.Open_Opportunity_Amount__c,5000);
        
        //Create new Task
        Task testTask = new Task();
        testTask.Subject='TestTask';
        testTask.status = 'Not Started';
        testTask.ActivityDate=System.Today();
        testTask.IsReminderSet = true;
        testTask.ReminderDateTime = System.now();
        testTask.Priority='Normal';
        testTask.WhatId=testAccount.Id;
        database.insert(testTask);
        
        
    }
}