@isTest
public class opportunityAmountRollTestClass
{
    static TestMethod void updateAccount()
    {
        
        //Create a new Account    
        Account testAccount = new Account();
        testAccount.Name = 'TestAccount';
        testAccount.Credit_limit_approved_Amount__c=400;
        database.insert(testAccount);
        
        //Create a new Opportunity
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'TestOpportunity';
        testOpportunity.Accountid = testAccount.id;
        testOpportunity.CloseDate = date.today();
        testOpportunity.StageName = 'Initial contact';
        testOpportunity.Amount=4000;
        database.insert(testOpportunity);
                
        //Create a new Product
        Product2 testProduct =new Product2();
        testProduct.Name = 'TestProduct';
        testProduct.IsActive=true;
        database.insert(testProduct);
		
        // Create a pricebook entry for custom pricebook
        PricebookEntry testPricebookEntry = new PricebookEntry();
        testPricebookEntry.UseStandardPrice = false;
        testPricebookEntry.Pricebook2Id='01s90000000ZcDh';//testPricebook.id;
        testPricebookEntry.Product2Id=testProduct.id;
        testPricebookEntry.IsActive=true;
        testPricebookEntry.UnitPrice=100.0;
        database.insert(testPricebookEntry);
        
        //Create new Opportunity Line Item 
        OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
        testOpportunityLineItem.UnitPrice=200;
        testOpportunityLineItem.Quantity=3;
        testOpportunityLineItem.OpportunityId=testOpportunity.Id;
        testOpportunityLineItem.PricebookEntryId=testPricebookEntry.id;
        database.insert(testOpportunityLineItem);
        
        //Update Opportunity Line Item 
        OpportunityLineItem updateRec = new OpportunityLineItem(id=testOpportunityLineItem.id);
        updateRec.Quantity=4;
        database.update(updateRec);
                
        //Update Opportunity      
        Opportunity testOpportunity2 = new Opportunity();
        testOpportunity2.Id = testOpportunity.id;
        testOpportunity2.CloseDate = date.today();
        testOpportunity2.StageName = 'Initial contact';
        database.update(testOpportunity2);
        
        //Create new Task
        Task testTask = new Task();
        testTask.Subject='TestTask';
        testTask.status = 'Not Started';
        testTask.ActivityDate=System.Today();
        testTask.IsReminderSet = true;
        testTask.ReminderDateTime = System.now();
        testTask.Priority='Normal';
        testTask.WhatId=testAccount.Id;
        database.insert(testTask);
        
        //Update Opportunity Line Item 
        OpportunityLineItem updateRec2 = new OpportunityLineItem(id=updateRec.id);
        database.delete(updateRec);
    }
    static TestMethod void updateAccount2()
    {
          //Create new Account
        Account testAccount = new Account();
        testAccount.Name = 'TestAccount';
        testAccount.Open_Opportunity_Total_Amount__c=NULL;
        database.insert(testAccount);
        
        //Create new Opportunity 
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'TestOpportunity';
        testOpportunity.Accountid = testAccount.id;
        testOpportunity.CloseDate = date.today();
        testOpportunity.StageName = 'Accepted';
        testOpportunity.Amount=4000;
        database.insert(testOpportunity);      
    	
        //Create a new Product
        Product2 testProduct =new Product2();
        testProduct.Name = 'TestProduct';
        testProduct.IsActive=true;
        database.insert(testProduct);
		
        // Create a pricebook entry for custom pricebook
        PricebookEntry testPricebookEntry = new PricebookEntry();
        testPricebookEntry.UseStandardPrice = false;
        testPricebookEntry.Pricebook2Id='01s90000000ZcDh';//testPricebook.id;
        testPricebookEntry.Product2Id=testProduct.id;
        testPricebookEntry.IsActive=true;
        testPricebookEntry.UnitPrice=100.0;
        database.insert(testPricebookEntry);
        
        //Create new Opportunity Line Item 
        OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem();
        testOpportunityLineItem.UnitPrice=200;
        testOpportunityLineItem.Quantity=3;
        testOpportunityLineItem.OpportunityId=testOpportunity.Id;
        testOpportunityLineItem.PricebookEntryId=testPricebookEntry.id;
        database.insert(testOpportunityLineItem);
        
        //Update Opportunity Line Item 
        OpportunityLineItem updateRec = new OpportunityLineItem(id=testOpportunityLineItem.id);
        updateRec.Quantity=4;
        database.update(updateRec);
                
        //Update Opportunity      
        Opportunity testOpportunity2 = new Opportunity();
        testOpportunity2.Id = testOpportunity.id;
        testOpportunity2.CloseDate = date.today();
        testOpportunity2.StageName = 'Initial contact';
        database.update(testOpportunity2);
        
        //Create new Task
        Task testTask = new Task();
        testTask.Subject='TestTask';
        testTask.status = 'Not Started';
        testTask.ActivityDate=System.Today();
        testTask.IsReminderSet = true;
        testTask.ReminderDateTime = System.now();
        testTask.Priority='Normal';
        testTask.WhatId=testAccount.Id;
        database.insert(testTask);
    
    }
}