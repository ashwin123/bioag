@isTest
public class OpportunityRollUpDeductTestClass
{
    static TestMethod void updateAccount()
    {
        //Create new Account
        Account testAccount = new Account();
        testAccount.Name = 'TestAccount';
        testAccount.Credit_limit_approved_Amount__c=4000;
        database.insert(testAccount);
        
        //Create new Opportunity 
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'TestOpportunity';
        testOpportunity.Accountid = testAccount.id;
        testOpportunity.CloseDate = date.today();
        testOpportunity.StageName = 'Accepted';
        testOpportunity.Amount=4000;
        database.insert(testOpportunity);
        testOpportunity.Amount=3000;
        update testOpportunity;
        
        Opportunity testOpportunity2 = new Opportunity(id=testOpportunity.id);
        delete testOpportunity2;
        
        //Update Opportunity 
       /* Opportunity testOpportunity3 = new Opportunity();
        testOpportunity3.Id = testOpportunity.id;
        testOpportunity3.CloseDate = date.today();
        testOpportunity3.StageName = 'Rejected';
        testOpportunity3.Amount=3000;
        database.update(testOpportunity3);*/
    }
   static TestMethod void updateAccount2()
    {
          //Create new Account
        Account testAccount = new Account();
        testAccount.Name = 'TestAccount';
        testAccount.Open_Opportunity_Total_Amount__c=NULL;
        database.insert(testAccount);
        
        //Create new Opportunity 
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.Name = 'TestOpportunity';
        testOpportunity.Accountid = testAccount.id;
        testOpportunity.CloseDate = date.today();
        testOpportunity.StageName = 'Accepted';
        testOpportunity.Amount=4000;
        database.insert(testOpportunity);      
    	testOpportunity.Amount=3000;
    	update testOpportunity;
    
    
    }
}